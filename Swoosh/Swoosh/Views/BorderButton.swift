//
//  BorderButton.swift
//  Swoosh
//
//  Created by Khaled Esam on 9/27/17.
//  Copyright © 2017 Khaled Esam. All rights reserved.
//

import UIKit

class BorderButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.borderWidth = 2.0
        layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
    }

}
